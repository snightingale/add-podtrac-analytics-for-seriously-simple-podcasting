=== Add Podtrac Analytics for Seriously Simple Podcasting ===
Contributors: snightingale
Tags: podcast, podtrac, Seriously Simple Podcasting, podtrac
Requires at least: 4.4
Tested up to: 5.3.2
Stable tag: 0.1.2
Requires PHP: 5.6
Author URI: https://gitlab.com/snightingale/add-podtrac-analytics-for-seriously-simple-podcasting
Donate link: https://www.paypal.me/darthvader666uk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Inspired by [Podtrac & Seriously Simple Podcasting](https://wordpress.org/plugins/add-podtrac-to-seriously-simple-podcasting/), This Wordpress plugin allows you to add Podtrac analytics to your podcast media files when using the [Seriously Simple Podcasting](http://seriouslysimplepodcasting.com) Wordpress plugin.

== Installation ==

This can be installed from the Wordpress Plugin directory, Maunal install from the Wordpress Plugin interface or via FTP.

* Wordpress Plugin directory: Search for `Add Podtrac Analytics for Seriously Simple Podcasting`
* Download the release from Wordpress Plugin directory or the [GitLab page](https://gitlab.com/snightingale/add-podtrac-analytics-for-seriously-simple-podcasting)
* Connect via FTP, extract the zip and upload the `podtrac-analytics-for-seriously-simples-podcasting` folder to `/wp-content/plugins/` directory.

Once Installed, this can then be actived on the plugins page.

== Frequently Asked Questions ==

1. By Clicking save and the cache box ticked, this will refresh the RSS feed.

== Usage ==

To use this plugin (once activated) Navigate to the `Seriously Simple Podcasting` Settings option and choose the `Podtrac Analytics` Tab on top to active.

Then you will need to tick `Enable Podtrac Episode Measurement Service` and recommend ticking `Refresh RSS Cache` so the results are seen straight away.

== Screenshots ==

1. This is the layout of the plugin

== Changelog ==

= 0.1.2 =
* create_function() is deprecated and updated to  Anonymous Function

= 0.1.1 =
* Added ability to add languages to the plugin

= 0.1.0 =
* Initial Release

== Upgrade Notice ==

= 0.1.2 =
* create_function() is deprecated and updated to  Anonymous Function